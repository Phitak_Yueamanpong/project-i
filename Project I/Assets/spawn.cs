using System;
using Spaceship;
using UnityEngine;
using UnityEngine.SceneManagement;
using Manager;

public class spawn : MonoBehaviour
{
    [SerializeField] private PlayerSpaceship playerSpaceship;
    [SerializeField] private EnemySpaceship enemySpaceship;
    [SerializeField] private GameObject respawn;
    [SerializeField] private int playerSpaceshipHp;
    [SerializeField] private int playerSpaceshipMoveSpeed;
    [SerializeField] private int enemySpaceshipHp;
    [SerializeField] private int enemySpaceshipMoveSpeed;
    
    [HideInInspector]
        public PlayerSpaceship spawnedPlayerShip;
        
        
        private void Awake()
        {
            SpawnPlayerSpaceship();
            SpawnEnemySpaceship();
        }
        
        
        private void SpawnPlayerSpaceship()
        {
            spawnedPlayerShip = Instantiate(playerSpaceship);
            spawnedPlayerShip.Init(playerSpaceshipHp, playerSpaceshipMoveSpeed);
            spawnedPlayerShip.OnExploded += OnPlayerSpaceshipExploded;
        }

        private void OnPlayerSpaceshipExploded()
        {
            SceneManager.LoadScene(3);
        }

        private void SpawnEnemySpaceship()
        {
            var spawnedEnemyShip = Instantiate(enemySpaceship);
            spawnedEnemyShip.Init(enemySpaceshipHp, enemySpaceshipMoveSpeed);
            spawnedEnemyShip.OnExploded += OnEnemySpaceshipExploded;
        }

        private void OnEnemySpaceshipExploded()
        {
            SceneManager.LoadScene(2);
        }

        
        
}
